package com.vauto.dork.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.vauto.dork.MyApp;
import com.vauto.dork.R;
import com.vauto.dork.models.data.Player;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Dialog fragment to create and edit a player's names.
 * Created by robert on 3/7/15.
 */
public class EditPlayerDialogFragment extends DialogFragment {
    public static final String TAG = EditPlayerDialogFragment.class.getSimpleName();

    TextView title;
    ProgressBar progressBar;
    @InjectView(R.id.first_name)
    EditText firstName;
    @InjectView(R.id.last_name)
    EditText lastName;
    @InjectView(R.id.nickname)
    EditText nickname;

    private static final String ARG_FIRST_NAME = "arg_first_name";
    private static final String ARG_LAST_NAME = "arg_last_name";
    private static final String ARG_NICKNAME = "arg_nickname";
    private static final String ARG_PROGRESS_VISIBILITY = "arg_progress_visibility";

    private Callbacks callbacks;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity())
                .setPositiveButton(android.R.string.ok, null);
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_loading_title, null);
        alertDialogBuilder.setCustomTitle(v);
        title = (TextView) v.findViewById(R.id.title);
        title.setText(R.string.new_player);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);

        v = LayoutInflater.from(getActivity()).inflate(R.layout.edit_player_dialog, null);
        ButterKnife.inject(this, v);

        boolean isProgressVisible = false;
        if (savedInstanceState != null) {
            firstName.setText(savedInstanceState.getString(ARG_FIRST_NAME, ""));
            lastName.setText(savedInstanceState.getString(ARG_LAST_NAME, ""));
            nickname.setText(savedInstanceState.getString(ARG_NICKNAME, ""));
            isProgressVisible = savedInstanceState.getInt(ARG_PROGRESS_VISIBILITY, View.GONE) == View.VISIBLE;
        }
        progressBar.setVisibility(isProgressVisible ? View.VISIBLE : View.GONE);

        AlertDialog alertDialog = alertDialogBuilder.create();

        final int margin = Math.round(getResources().getDimension(R.dimen.activity_horizontal_margin));
        alertDialog.setView(v, margin, 0, margin, 0);

        return alertDialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Button positiveButton = getPositiveButton();
        // disable the positive button if the progress bar is visible
        if (progressBar.getVisibility() == View.VISIBLE) {
            positiveButton.setEnabled(false);
        }
        // continue with setup.
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onAddNewPlayer()) {
                    dismiss();
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(ARG_FIRST_NAME, firstName.getText().toString());
        outState.putString(ARG_LAST_NAME, lastName.getText().toString());
        outState.putString(ARG_NICKNAME, nickname.getText().toString());
        outState.putInt(ARG_PROGRESS_VISIBILITY, progressBar.getVisibility());
        super.onSaveInstanceState(outState);
    }

    /**
     * Attempt to add a new player with the inputted information.
     *
     * @return True if the player is added, false if not.
     */
    private boolean onAddNewPlayer() {
        String firstNameStr = getValidText(firstName);
        if (firstNameStr == null) {
            return false;
        }

        String lastNameStr = getValidText(lastName);
        if (lastNameStr == null) {
            return false;
        }

        String nicknameStr = nickname.getText().toString().trim();

        final Player player = new Player(firstNameStr, lastNameStr, nicknameStr);
        saveInBackground(player);
        return false;
    }

    private void saveInBackground(final Player player) {
        final Button positiveButton = getPositiveButton();
        positiveButton.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);

        MyApp.getPlayersService().savePlayer(player, new Callback<Player>() {
            @Override
            public void success(Player player, Response response) {
                callbacks.onPlayerSaved(player);
                dismiss();
                done();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity(), R.string.error_saving, Toast.LENGTH_LONG).show();
                positiveButton.setEnabled(true);
                done();
            }

            private void done() {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private Button getPositiveButton() {
        return ((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_POSITIVE);
    }

    /**
     * Get valid text from the edit text field. If the text is not valid then null is returned.
     *
     * @param editText Text field to validate.
     * @return Returns valid text from the field or null if the text is not valid.
     */
    @Nullable
    private String getValidText(@NonNull EditText editText) {
        final String text = editText.getText().toString().trim();
        if (TextUtils.isEmpty(text)) {
            editText.requestFocus();
            Toast.makeText(getActivity(), R.string.enter_name_or_initial, Toast.LENGTH_SHORT).show();
            return null;
        }
        return text;
    }

    public void setCallbacks(Callbacks callbacks) {
        this.callbacks = callbacks;
    }

    public static interface Callbacks {
        /**
         * Called when the player is saved.
         *
         * @param player Player object that was saved.
         */
        public void onPlayerSaved(@NonNull Player player);
    }
}
