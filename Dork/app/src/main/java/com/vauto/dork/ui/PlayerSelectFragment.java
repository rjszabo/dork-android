package com.vauto.dork.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.vauto.dork.MyApp;
import com.vauto.dork.R;
import com.vauto.dork.adapter.PlayersListAdapter;
import com.vauto.dork.models.PlayerSortType;
import com.vauto.dork.models.data.Player;
import com.vauto.dork.utils.SimpleDividerItemDecoration;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Shows the games played history
 * Created by robert on 3/5/15.
 */
public class PlayerSelectFragment extends BaseListFragment<
        Player,
        PlayersListAdapter>
        implements EditPlayerDialogFragment.Callbacks, PlayersListAdapter.Callbacks {

    public static final String TAG = PlayerSelectFragment.TAG;
    private static final String ARG_SELECTED_PLAYERS = "arg_selected_players";
    private Callbacks callbacks;


    PlayersListAdapter adapter;

    @NonNull
    public static PlayerSelectFragment newInstance(@Nullable ArrayList<Player> selectedPlayers) {
        PlayerSelectFragment fragment = new PlayerSelectFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_SELECTED_PLAYERS, selectedPlayers);
        fragment.setArguments(args);
        return fragment;
    }

    public PlayerSelectFragment() {
        adapter = new PlayersListAdapter(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        callbacks = (Callbacks) activity;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        ((ActionBarActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_cancel);
        setHasOptionsMenu(true);

        if (savedInstanceState != null) {
            adapter.onRestoreInstanceState(savedInstanceState);
        } else if (getArguments() != null) {
            adapter.preLoadedSelectedList(getArguments().<Player>getParcelableArrayList(ARG_SELECTED_PLAYERS));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.player_select, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = false;
        if (item.getItemId() == R.id.menu_select) {
            ArrayList<Player> selectedPlayers = adapter.getSelectedPlayersList();
            if (selectedPlayers.size() > 0) {
                handled = true;
                sortPlayers(selectedPlayers, item);
            }
        }
        return handled || super.onOptionsItemSelected(item);
    }

    private void sortPlayers(@NonNull ArrayList<Player> selectedPlayers, @NonNull final MenuItem item) {
        item.setEnabled(false);
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        final int month = calendar.get(Calendar.MONTH);
        final int year = calendar.get(Calendar.YEAR);
        MyApp.getPlayersService().sortPlayers(month, year, PlayerSortType.Board.ordinal(), selectedPlayers, new Callback<ArrayList<Player>>() {
            @Override
            public void success(ArrayList<Player> players, Response response) {
                callbacks.onPlayersSelected(players);
                done();
            }

            @Override
            public void failure(RetrofitError error) {
                done();
            }

            private void done() {
                item.setEnabled(true);
                getActivity().supportInvalidateOptionsMenu();
            }
        });
    }

    @Override
    protected void loadListFromNetwork() {
        MyApp.getPlayersService().getPlayers(
                true,
                new Callback<ArrayList<Player>>() {
                    @Override
                    public void success(ArrayList<Player> rankedPlayers, Response response) {
                        adapter.setList(rankedPlayers);
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
    }

    @NonNull
    @Override
    protected PlayersListAdapter getAdapter() {
        return adapter;
    }

    @Nullable
    @Override
    public View.OnClickListener getOnFabClickListener() {
        return onFabClickListener;
    }

    private View.OnClickListener onFabClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            EditPlayerDialogFragment dialogFragment = new EditPlayerDialogFragment();
            dialogFragment.setCallbacks(PlayerSelectFragment.this);
            dialogFragment.show(getFragmentManager(), EditPlayerDialogFragment.TAG);
        }
    };

    @Override
    public void onPlayerSaved(@NonNull Player player) {
        if (adapter.getList() != null) {
            ArrayList<Player> list = adapter.getList();
            list.add(player);
            adapter.setList(list);
        }
    }

    @Override
    public void onPlayerSelectionChange(int count) {
        setTitle(count);
    }

    private void setTitle(final int count) {
        if (getActivity() != null) {
            ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(String.valueOf(count));
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        adapter.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    public interface Callbacks {
        /**
         * Called when players are selected.
         *
         * @param selectedPlayersList List of selected players
         */
        public void onPlayersSelected(ArrayList<Player> selectedPlayersList);
    }
}
