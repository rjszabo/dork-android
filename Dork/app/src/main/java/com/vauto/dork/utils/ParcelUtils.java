package com.vauto.dork.utils;

import android.os.Parcel;

import java.util.Date;

public class ParcelUtils {
    private static final byte EXISTS = 0x01;
    private static final byte NOT_EXISTS = 0x00;

    public static void writeDate(Parcel parcel, Date date) {
        if (date == null) {
            parcel.writeByte(NOT_EXISTS);
        } else {
            parcel.writeByte(EXISTS);
            parcel.writeLong(date.getTime());
        }
    }

    public static Date readDate(Parcel source) {
        if (source.readByte() == EXISTS) {
            return new Date(source.readLong());
        }

        return null;
    }
}
