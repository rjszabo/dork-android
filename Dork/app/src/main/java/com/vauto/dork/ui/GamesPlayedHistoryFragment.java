package com.vauto.dork.ui;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.vauto.dork.MyApp;
import com.vauto.dork.R;
import com.vauto.dork.adapter.ListAdapter;
import com.vauto.dork.models.data.Game;
import com.vauto.dork.utils.Utils;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A fragment used to show the history of games played. This fragment allows users to create a new game.
 * Created by robert on 3/5/15.
 */
public class GamesPlayedHistoryFragment extends BaseListFragment<
        Game,
        GamesPlayedHistoryFragment.GamesPlayedHistoryAdapter> {

    public static final int REQ_GAME = 0;
    public static final String TAG = GamesPlayedHistoryFragment.TAG;

    GamesPlayedHistoryAdapter adapter;

    public GamesPlayedHistoryFragment() {
        adapter = new GamesPlayedHistoryAdapter();
    }

    @Override
    protected void loadListFromNetwork() {
        final Activity activity = getActivity();
        MyApp.getGamesService().getGames(new Callback<ArrayList<Game>>() {
            @Override
            public void success(ArrayList<Game> games, Response response) {
                adapter.setList(games);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(activity, R.string.error_getting_game_list, Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Error getting games: " + error.getKind().toString() + ": " + error);
            }
        });
    }

    @NonNull
    @Override
    protected GamesPlayedHistoryAdapter getAdapter() {
        return adapter;
    }

    @Nullable
    @Override
    public View.OnClickListener getOnFabClickListener() {
        return onFabClickListener;
    }

    private View.OnClickListener onFabClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(getActivity(), EditGameActivity.class);
            startActivityForResult(i, REQ_GAME);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == REQ_GAME) {
            Game game = data.getParcelableExtra(EditGameActivity.ARG_GAME);
            ArrayList<Game> games = adapter.getList();

            // update the game if it exists, add it if it is new
            boolean found = false;
            if (games != null) {
                for (Game existingGame : games) {
                    if (existingGame.getObjectId().equals(game.getObjectId())) {
                        // modified game, update data
                        found = true;
                        existingGame.fromModel(game);
                    }
                }
            } else {
                games = new ArrayList<>(1);
            }
            if (!found) {
                games.add(game);
            }
            adapter.setList(games);
        }
    }

    class GamesPlayedHistoryAdapter extends RecyclerView.Adapter<GamesPlayedHistoryAdapter.ViewHolder> implements ListAdapter<Game> {

        private ArrayList<Game> gamesList;

        public class ViewHolder extends RecyclerView.ViewHolder {
            @InjectView(R.id.winner)
            TextView winner;
            @InjectView(R.id.date_played)
            TextView datePlayed;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.inject(this, itemView);
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.game_history_list_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Game game = gamesList.get(position);
            holder.datePlayed.setText(Utils.getFormattedDate(game.getDatePlayed()));
            holder.winner.setText(game.getWinner().getAbbrDisplayName());
        }

        @Override
        public int getItemCount() {
            return gamesList == null ? 0 : gamesList.size();
        }

        @Nullable
        @Override
        public ArrayList<Game> getList() {
            return gamesList;
        }

        @Nullable
        @Override
        public void setList(ArrayList<Game> list) {
            gamesList = list;
            notifyDataSetChanged();
        }
    }
}
