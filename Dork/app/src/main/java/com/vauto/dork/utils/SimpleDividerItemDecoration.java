package com.vauto.dork.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.vauto.dork.R;

/**
 * Used to create a divider for a {@link android.support.v7.widget.RecyclerView}.
 *
 * @author polbins
 * @see <a href="https://gist.github.com/polbins/e37206fbc444207c0e92">polbins' gist</a>
 */
public class SimpleDividerItemDecoration extends RecyclerView.ItemDecoration {
    private Drawable mDivider;

    /**
     * Create a divider decoration with a default divider of {@link com.vauto.dork.R.drawable#list_divider}.
     *
     * @param context
     */
    public SimpleDividerItemDecoration(@NonNull Context context) {
        this(context, R.drawable.list_divider);
    }

    /**
     * Create a divider with a specified divider.
     *
     * @param context       Current context.
     * @param drawableResId Drawable resource Id to use for the divider.
     */
    public SimpleDividerItemDecoration(@NonNull Context context, @DrawableRes int drawableResId) {
        mDivider = context.getResources().getDrawable(drawableResId);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }
}