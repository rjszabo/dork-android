package com.vauto.dork.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.EditText;
import android.widget.TextView;

import com.vauto.dork.R;
import com.vauto.dork.models.data.GamePlayer;
import com.vauto.dork.models.data.Player;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Adapter to show players and their stats from a game.
 * Created by robert on 3/8/15.
 */
public class GamePlayersListAdapter
        extends RecyclerView.Adapter<GamePlayersListAdapter.ViewHolder>
        implements ListAdapter<GamePlayer> {

    private Callbacks callbacks;
    private ArrayList<GamePlayer> playersList;
    private SparseArray<GamePlayer> placedPlayers;
    private static final Interpolator INTERPOLATOR = new AccelerateInterpolator();
    private boolean enabled;

    public GamePlayersListAdapter(Callbacks callbacks) {
        this.callbacks = callbacks;
        placedPlayers = new SparseArray<>();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.text1)
        TextView name1;
        @InjectView(R.id.text2)
        TextView name2;
        @InjectView(R.id.points)
        EditText points;
        @InjectView(R.id.first_place)
        View firstPlace;
        @InjectView(R.id.second_place)
        View secondPlace;
        @InjectView(R.id.third_place)
        View thirdPlace;

        GamePlayer gamePlayer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            firstPlace.setOnClickListener(onClickListener);
            secondPlace.setOnClickListener(onClickListener);
            thirdPlace.setOnClickListener(onClickListener);
            points.addTextChangedListener(textWatcher);
        }

        private TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String input = s.toString();
                if (TextUtils.isEmpty(input)) {
                    gamePlayer.setPoints(0);
                } else if ((input = input.trim()).equals("-")) {
                    gamePlayer.setPoints(0);
                } else {
                    gamePlayer.setPoints(Integer.valueOf(input));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };

        private View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int place = Integer.parseInt((String) v.getTag());

                GamePlayer oldPlacedPlayers = placedPlayers.get(place);
                if (oldPlacedPlayers != null) {
                    if (oldPlacedPlayers.equals(gamePlayer)) {
                        int key = gamePlayer.getRank();
                        placedPlayers.delete(place);
                        if (key == place) {
                            gamePlayer.setRank(GamePlayer.PLACE_UNKNOWN);
                        } else {
                            gamePlayer.setRank(place);
                            placedPlayers.put(place, gamePlayer);
                        }
                    } else {
                        oldPlacedPlayers.setRank(GamePlayer.PLACE_UNKNOWN);
                        gamePlayer.setRank(place);
                        remove(gamePlayer);
                        placedPlayers.put(place, gamePlayer);
                    }
                } else {
                    remove(gamePlayer);
                    gamePlayer.setRank(place);
                    placedPlayers.put(place, gamePlayer);
                }
                callbacks.moneyChange();
                notifyDataSetChanged();
            }

            private void remove(GamePlayer gamePlayer) {
                int index = placedPlayers.indexOfValue(gamePlayer);
                if (index >= 0) {
                    placedPlayers.removeAt(index);
                }
            }
        };
    }

    @Override
    public GamePlayersListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.player_list_item, parent, false);
        return new GamePlayersListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(GamePlayersListAdapter.ViewHolder holder, int position) {
        holder.itemView.setEnabled(enabled);
        GamePlayer gamePlayer = getItem(position);
        holder.gamePlayer = gamePlayer;
        setActivated(holder, gamePlayer.getRank());

        Player player = gamePlayer.getPlayer();
        assert player != null;

        final String displayName = player.getAbbrDisplayName();
        holder.name1.setText(displayName);
        if (TextUtils.isEmpty(player.getNickname())) {
            // no nickname
            holder.name2.setVisibility(View.GONE);
        } else {
            // nickname exists
            holder.name2.setText(player.getFullName());
            holder.name2.setVisibility(View.VISIBLE);
        }
        holder.points.setText(String.valueOf(gamePlayer.getPoints()));
    }

    /**
     * Set the activated state if relevant
     *
     * @param holder Holder with views
     * @param place  first, second, third, etc.
     */
    private void setActivated(ViewHolder holder, final int place) {
        switch (place) {
            case 1:
                setActivated(holder.firstPlace, true);
                shrink(holder.secondPlace);
                shrink(holder.thirdPlace);
                break;
            case 2:
                shrink(holder.firstPlace);
                setActivated(holder.secondPlace, true);
                shrink(holder.thirdPlace);
                break;
            case 3:
                shrink(holder.firstPlace);
                shrink(holder.secondPlace);
                setActivated(holder.thirdPlace, true);
                break;
            default:
                if (placedPlayers.get(1) != null) {
                    shrink(holder.firstPlace);
                } else {
                    setActivated(holder.firstPlace, false);
                }
                if (placedPlayers.get(2) != null) {
                    shrink(holder.secondPlace);
                } else {
                    setActivated(holder.secondPlace, false);
                }
                if (placedPlayers.get(3) != null) {
                    shrink(holder.thirdPlace);
                } else {
                    setActivated(holder.thirdPlace, false);
                }
        }
    }

    public void setActivated(@NonNull final View v, final boolean activated) {
        v.setActivated(activated);
        final float scaleX;
        final float scaleY;
        if (activated) {
            scaleX = scaleY = 1.12f;
        } else {
            scaleX = scaleY = 1.0f;
        }
        v.animate()
                .setDuration(0) // TODO Turning animation off. Animation causes weird behavior with a large list of names.
                .setInterpolator(INTERPOLATOR)
                .scaleX(scaleX)
                .scaleY(scaleY)
                .start();
    }

    public void shrink(@NonNull View v) {
        v.setActivated(false);
        final float scaleX = 0.7f;
        final float scaleY = 0.7f;
        v.animate()
                .setDuration(0) // TODO Turning animation off. Animation causes weird behavior with a large list of names.
                .setInterpolator(INTERPOLATOR)
                .scaleX(scaleX)
                .scaleY(scaleY)
                .start();
    }

    @Override
    public int getItemCount() {
        return playersList == null ? 0 : playersList.size();
    }

    @NonNull
    GamePlayer getItem(final int position) {
        return playersList.get(position);
    }

    @Nullable
    @Override
    public ArrayList<GamePlayer> getList() {
        return playersList;
    }

    @Override
    public void setList(@Nullable ArrayList<GamePlayer> list) {
        this.playersList = list;
        placedPlayers.clear();
        if (list != null) {
            for (GamePlayer gamePlayer : list) {
                switch (gamePlayer.getRank()) {
                    case 1:
                        placedPlayers.put(1, gamePlayer);
                        break;
                    case 2:
                        placedPlayers.put(2, gamePlayer);
                        break;
                    case 3:
                        placedPlayers.put(3, gamePlayer);
                        break;
                }
            }
        }
        notifyDataSetChanged();
    }

    /**
     * "In The Money" means a player is in the top three.
     *
     * @return returns true if three players are In the Money!
     */
    public boolean hasThreeInTheMoney() {
        return placedPlayers.size() >= 3 &&
                placedPlayers.get(1) != null &&
                placedPlayers.get(2) != null &&
                placedPlayers.get(3) != null;
    }

    @Nullable
    public Player getPlayerInFirstPlace() {
        GamePlayer gamePlayer = placedPlayers.get(1);
        return gamePlayer != null ? gamePlayer.getPlayer() : null;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public interface Callbacks {
        /**
         * Called when places are added or removed from the top three.
         */
        public void moneyChange();
    }
}