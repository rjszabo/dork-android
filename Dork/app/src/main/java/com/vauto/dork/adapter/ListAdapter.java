package com.vauto.dork.adapter;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import java.util.ArrayList;

/**
 * Intended for use by list adapters that can be used by {@link com.vauto.dork.ui.BaseListFragment}.
 * Created by robert on 3/7/15.
 *
 * @param <L> Type of list.
 */
public interface ListAdapter<L extends Parcelable> {
    /**
     * Get the list of items used.
     *
     * @return List being used.
     */
    @Nullable
    public ArrayList<L> getList();

    /**
     * Set the list to be used by the adapter.
     *
     * @param list List to use.
     */
    public void setList(@Nullable ArrayList<L> list);
}
