package com.vauto.dork.models.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Model for a player in a game. This can have game stats associated with it such as points.
 * Created by robert on 3/8/15.
 */
public class GamePlayer extends BaseModel<GamePlayer> implements Parcelable {
    /**
     * Used when the rank is unknown or hasn't been marked.
     */
    public static final int PLACE_UNKNOWN = 0;
    private Player player;
    private int points;
    private int rank;

    public GamePlayer() {
        super();
    }

    public GamePlayer(Player player, int points) {
        this.player = player;
        this.points = points;
    }

    public GamePlayer(Parcel source) {
        super(source);
        player = source.readParcelable(Player.class.getClassLoader());
        points = source.readInt();
        rank = source.readInt();
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public void fromModel(GamePlayer model) {
        super.fromModel(model);
        player = model.player;
        points = model.points;
        rank = model.rank;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(player, flags);
        dest.writeInt(points);
        dest.writeInt(rank);
    }

    public static final Creator<GamePlayer> CREATOR = new Creator<GamePlayer>() {
        @Override
        public GamePlayer createFromParcel(Parcel source) {
            return new GamePlayer(source);
        }

        @Override
        public GamePlayer[] newArray(int size) {
            return new GamePlayer[0];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        GamePlayer that = (GamePlayer) o;

        if (points != that.points) return false;
        if (rank != that.rank) return false;
        return !(player != null ? !player.equals(that.player) : that.player != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (player != null ? player.hashCode() : 0);
        result = 31 * result + points;
        result = 31 * result + rank;
        return result;
    }

    @Override
    public String toString() {
        return "GamePlayer{" +
                "player=" + player +
                ", points=" + points +
                ", rank=" + rank +
                "} " + super.toString();
    }
}
