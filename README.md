#Process
Fork this repo and create pull requests back to the main repository.
##debug vs release builds
Debug builds point to the dev app server. This flavor is to be used for dev and test effort.  
The release build points to the release server. Release builds should be distributed. **Do Not test** with a release build. Use the debug build for that.

#Account Info
username: `vautodork@gmail.com`  
password: `uberdork`  
This account is used for gmail, Trello, pretty much anything that this account should be associated with. If you have a personal account for a tool, for example Trello, then feel free to login as the `vautodork` user and invite yourself, or give yourself access.

#Trello
https://trello.com/b/Es8lXpIL/dork-android

# API Documentation
http://docs.dorkapi.apiary.io/